package com.thebeards.inshadow.imageprocessing.Multithreading.PixelPlacementEffects;

import com.thebeards.inshadow.imageprocessing.Multithreading.BitmapSolverSlave;

/**
 * Created by INshadow on 03.11.2017.
 */

public class BSSCosine extends BitmapSolverSlave {

    @Override
    protected void pixel(int x, int y) {

        int ny = (int) (y + (((1d + Math.cos(Math.toRadians(x))) * height * 0.025d) % height)) % height;
        presentingSet[x + y*width] = workingSet[x + ny*width];
    }
}
