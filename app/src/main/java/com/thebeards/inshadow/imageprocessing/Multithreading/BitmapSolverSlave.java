package com.thebeards.inshadow.imageprocessing.Multithreading;

import android.util.Log;

/**
 * Created by INshadow on 09.10.2017.
 */

public class BitmapSolverSlave extends Thread {

    protected int[] workingSet;
    protected int[] presentingSet;

    protected Object[] data;

    protected int top;
    protected int bottom;
    protected int left;
    protected int right;
    protected int width;
    protected int height;

    public BitmapSolverSlave(){

    }

    public BitmapSolverSlave(Object[] data){
        this.data = data;
    }

    public void SetSlice(int[] input, int output[], int w, int h, int l, int t, int r, int b){
        workingSet = input;
        presentingSet = output;
        top = t;
        bottom = b;
        left = l;
        right = r;
        width = w;
        height = h;
    }

    @Override
    public void run() {
        try {
            PreRun();
            for (int j = top; j < bottom; j++) {
                for (int i = left; i < right; i++) {
                    pixel(i, j);
                }
            }
        } catch (Exception e){
            Log.e("BitmapSolverSlave: ",e.getMessage());
        } finally {
            PostRun();
        }
    }

    public void Prepare(){
        Log.w("BitmapSolverSlave","Prepared to execution");
    }

    public void Finalize(){
        Log.w("BitmapSolverSlave","Finishing execution");
    }

    protected void PreRun(){
        Log.i("PreRun","calling starters");
    }

    protected void PostRun(){
        Log.i("PostRun","calling finalizers");
    }

    protected void pixel(int x, int y) throws Exception {

    }
}
