package com.thebeards.inshadow.imageprocessing.Multithreading.ColorChannelEffects;

import com.thebeards.inshadow.imageprocessing.ColorSupport.ColorSupport;
import com.thebeards.inshadow.imageprocessing.Multithreading.BitmapSolverSlave;

import java.util.Arrays;

/**
 * Created by INshadow on 10.11.2017.
 */

public class BSSMedian extends BitmapSolverSlave {

    int block_size;
    int disp;

    int tx, ty;
    int[][] median_channels;
    int[] values;

    int[] mrc;
    int[] mgc;
    int[] mbc;
    int mr, mg, mb, ma;

    public BSSMedian(Object[] data){
        super(data);
        block_size = (int) super.data[0];
        disp = block_size / 2;
        median_channels = new int[block_size*block_size][4];
        values = new int[block_size*block_size];
        mrc = new int[block_size * block_size];
        mgc = new int[block_size * block_size];
        mbc = new int[block_size * block_size];
    }

    @Override
    protected void pixel(int x, int y){
        for (int j = 0; j < block_size; j++) {
            for (int i = 0; i < block_size; i++) {
                tx = x - disp + i;
                ty = y - disp + j;
                if (tx < 0 || ty < 0 || tx >= width || ty >= height)
                    continue;
                values[i + (j * block_size)] = workingSet[tx + (ty*width)];

            }
        }

        for (int i = 0; i < block_size * block_size; i++) {
            median_channels[i] = ColorSupport.ToComponents(values[i]);
        }


        ma = ColorSupport.ToComponents(workingSet[x + (y*width)])[3];

        for (int i = 0; i < block_size * block_size; i++) {
            median_channels[i] = ColorSupport.ToComponents(values[i]);
            mrc[i] = median_channels[i][0];
            mgc[i] = median_channels[i][1];
            mbc[i] = median_channels[i][2];
        }

        Arrays.sort(mrc);
        Arrays.sort(mgc);
        Arrays.sort(mbc);

        mr = mrc[block_size * block_size / 2];
        mg = mgc[block_size * block_size / 2];
        mb = mbc[block_size * block_size / 2];

        presentingSet[x + (y*width)] = ColorSupport.ToColor(mr,mg,mb,ma);
    }

}
