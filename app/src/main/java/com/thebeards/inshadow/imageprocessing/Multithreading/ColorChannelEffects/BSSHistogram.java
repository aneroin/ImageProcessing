package com.thebeards.inshadow.imageprocessing.Multithreading.ColorChannelEffects;

import com.thebeards.inshadow.imageprocessing.ColorSupport.ColorSupport;
import com.thebeards.inshadow.imageprocessing.Multithreading.BitmapSolverSlave;

import java.util.Arrays;

/**
 * Created by INshadow on 10.11.2017.
 */

public class BSSHistogram extends BitmapSolverSlave {


    static int[] R_Histogram;
    static int[] G_Histogram;
    static int[] B_Histogram;

    int[] channels;

    protected synchronized void IncR(int i){
        R_Histogram[i] += 1;
    }

    protected synchronized void IncG(int i){
        G_Histogram[i] += 1;
    }

    protected synchronized void IncB(int i){
        B_Histogram[i] += 1;
    }

    public BSSHistogram(){
        channels = new int[4];
        R_Histogram = new int[256];
        G_Histogram = new int[256];
        B_Histogram = new int[256];
    }

    @Override
    protected void pixel(int x, int y){
        channels = ColorSupport.ToComponents(workingSet[x + (y*width)]);
        IncR(channels[0]);
        IncG(channels[1]);
        IncB(channels[2]);
        presentingSet[x + (y*width)] = workingSet[x + (y*width)];
    }

    @Override
    public void Finalize(){
        super.PostRun();

        int maxR, maxG, maxB;
        maxR = maxG = maxB = 0;
        for (int i = 0; i < 255; i++){
            maxR = R_Histogram[i] > maxR ? R_Histogram[i] : maxR;
            maxG = G_Histogram[i] > maxG ? G_Histogram[i] : maxG;
            maxB = B_Histogram[i] > maxB ? B_Histogram[i] : maxB;
        }

        int globalScaleBias = maxR > maxG ? maxR : maxG;
            globalScaleBias = globalScaleBias > maxB ? globalScaleBias : maxB;

        if (maxR > 0)
            for (int i = 0; i < 128; i++) {
                for (int j = 0; j < ((R_Histogram[i] / (globalScaleBias + 0.0d)) * 256); j++) {
                    paint(i, height - (j + 1), ColorSupport.ToColor(255, 0, 0));
                }
            }

        if (maxG > 0)
            for (int i = 0; i < 128; i++) {
                for (int j = 0; j < ((G_Histogram[i] / (globalScaleBias + 0.0d)) * 256); j++) {
                    paint(i + 128, height - (j + 1), ColorSupport.ToColor(0, 255, 0));
                }
            }

        if (maxB > 0)
            for (int i = 0; i < 128; i++) {
                for (int j = 0; j < ((B_Histogram[i] / (globalScaleBias + 0.0d)) * 256); j++) {
                    paint(i + 256, height - (j + 1), ColorSupport.ToColor(0, 0, 255));
                }
            }
    }

    protected void paint(int x, int y, int color){
        presentingSet[x + (y*width)] = color;
    }

}
