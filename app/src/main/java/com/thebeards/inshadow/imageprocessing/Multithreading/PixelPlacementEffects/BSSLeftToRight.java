package com.thebeards.inshadow.imageprocessing.Multithreading.PixelPlacementEffects;

import com.thebeards.inshadow.imageprocessing.ColorSupport.ColorSupport;
import com.thebeards.inshadow.imageprocessing.Multithreading.BitmapSolverSlave;

/**
 * Created by INshadow on 14.10.2017.
 */

public class BSSLeftToRight extends BitmapSolverSlave {

    @Override
    protected void pixel(int x, int y){
        presentingSet[(width- x - 1) + y*width] = workingSet[x + y*width];
    }

}
