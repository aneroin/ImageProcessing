package com.thebeards.inshadow.imageprocessing.Multithreading.ColorChannelEffects;

import com.thebeards.inshadow.imageprocessing.ColorSupport.ColorSupport;
import com.thebeards.inshadow.imageprocessing.Multithreading.BitmapSolverSlave;

/**
 * Created by INshadow on 03.11.2017.
 */

public class BSSQuantum extends BitmapSolverSlave {

    private static int _quants = 2;
    private static int _qCoeff;

    @Override
    public void Prepare(){
        super.Prepare();
        _qCoeff = 256 / _quants;
    }

    @Override
    protected void pixel(int x, int y){
        int channels[] = ColorSupport.ToComponents(workingSet[x + (y*width)]);
        presentingSet[x + (y*width)] = ColorSupport.ToColor(
                _qCoeff * (channels[0] / _qCoeff) + _qCoeff / 2,
                _qCoeff * (channels[1] / _qCoeff) + _qCoeff / 2,
                _qCoeff * (channels[2] / _qCoeff) + _qCoeff / 2,
                channels[3]);
    }
}
