package com.thebeards.inshadow.imageprocessing.Multithreading;

/**
 * Created by INshadow on 22.10.2017.
 */

public class DelayedRunner {

    protected AsyncRunner runner;
    protected Class filter;

    public DelayedRunner(AsyncRunner runner, Class filter){
        this.runner = runner;
        this.filter = filter;
    }

    public void Execute() throws Exception{
        runner.execute(filter);
    }
}
