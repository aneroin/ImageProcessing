package com.thebeards.inshadow.imageprocessing;

import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.Toast;

import com.thebeards.inshadow.imageprocessing.Multithreading.AsyncRunner;
import com.thebeards.inshadow.imageprocessing.Multithreading.BitmapSolverSlave;
import com.thebeards.inshadow.imageprocessing.Multithreading.ColorChannelEffects.BSSAbberation;
import com.thebeards.inshadow.imageprocessing.Multithreading.ColorChannelEffects.BSSBlackWhite;
import com.thebeards.inshadow.imageprocessing.Multithreading.ColorChannelEffects.BSSBlueFilter;
import com.thebeards.inshadow.imageprocessing.Multithreading.ColorChannelEffects.BSSEqualize;
import com.thebeards.inshadow.imageprocessing.Multithreading.ColorChannelEffects.BSSGreenFilter;
import com.thebeards.inshadow.imageprocessing.Multithreading.ColorChannelEffects.BSSHistogram;
import com.thebeards.inshadow.imageprocessing.Multithreading.ColorChannelEffects.BSSMedian;
import com.thebeards.inshadow.imageprocessing.Multithreading.ColorChannelEffects.BSSQuantum;
import com.thebeards.inshadow.imageprocessing.Multithreading.ColorChannelEffects.BSSRedFilter;
import com.thebeards.inshadow.imageprocessing.Multithreading.ColorChannelEffects.BSSTintWarm;
import com.thebeards.inshadow.imageprocessing.Multithreading.ConvolutionEffects.BSSKernel;
import com.thebeards.inshadow.imageprocessing.Multithreading.DelayedRunner;
import com.thebeards.inshadow.imageprocessing.Multithreading.LuminanceEffects.BSSGammaCompression;
import com.thebeards.inshadow.imageprocessing.Multithreading.LuminanceEffects.BSSGammaExpansion;
import com.thebeards.inshadow.imageprocessing.Multithreading.PixelPlacementEffects.BSSCosine;
import com.thebeards.inshadow.imageprocessing.Multithreading.PixelPlacementEffects.BSSLeftToRight;
import com.thebeards.inshadow.imageprocessing.Multithreading.PixelPlacementEffects.BSSRotate90;
import com.thebeards.inshadow.imageprocessing.Multithreading.PixelPlacementEffects.BSSRotateTheta;
import com.thebeards.inshadow.imageprocessing.Multithreading.PixelPlacementEffects.BSSSine;
import com.thebeards.inshadow.imageprocessing.Multithreading.PixelPlacementEffects.BSSZoomIn;
import com.thebeards.inshadow.imageprocessing.Multithreading.PixelPlacementEffects.BSSZoomOut;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Queue;
import java.util.concurrent.LinkedTransferQueue;

public class MainActivity extends AppCompatActivity {

    static int step = 0;

    static int bitmap = 0;

    static int width = 512;
    static int height = 512;

    AbstractList<Class> filtersSimple;
    AbstractList<Pair<Pair<Class, String>,Object[]>> filtersComplex;

    protected static boolean isProcessing;
    public static synchronized void SetProcessing(boolean state){
        isProcessing = state;
        if (state == false){
            if (!Runners.isEmpty()){
                try {
                    Runners.remove().Execute();
                } catch (Exception e){
                    System.out.print(e.getMessage());
                }
            }
        }
    }

    Bitmap[] sourceBitmaps;
    Button[] controlButtons;
    Button[] menuButtons;
    ArrayList<ImageView> views;

    static Queue<DelayedRunner> Runners;

    GridLayout imageViewsGrid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LinearLayout mainLayout = new LinearLayout(this);
        mainLayout.setOrientation(LinearLayout.HORIZONTAL);

        ScrollView buttonsScroller = new ScrollView(this);
        buttonsScroller.setVerticalScrollBarEnabled(true);

        TableLayout controlButtonsTable = new TableLayout(this);
        controlButtonsTable.setOrientation(TableLayout.VERTICAL);

        ScrollView imageViewsScroller = new ScrollView(this);
        imageViewsScroller.setVerticalScrollBarEnabled(true);
        imageViewsScroller.setHorizontalScrollBarEnabled(true);


        imageViewsGrid = new GridLayout(this);
        imageViewsGrid.setOrientation(GridLayout.VERTICAL);
        imageViewsGrid.setColumnCount(1);

        Runners = new LinkedTransferQueue<>();
        views = new ArrayList<>();

        sourceBitmaps = new Bitmap[13];
        sourceBitmaps[0] = BitmapFactory.decodeResource(getResources(), R.drawable.airplane256);
        sourceBitmaps[1] = BitmapFactory.decodeResource(getResources(), R.drawable.black_rectangles256);
        sourceBitmaps[2] = BitmapFactory.decodeResource(getResources(), R.drawable.cicada256);
        sourceBitmaps[3] = BitmapFactory.decodeResource(getResources(), R.drawable.cross_board256);
        sourceBitmaps[4] = BitmapFactory.decodeResource(getResources(), R.drawable.five_rectangles256);
        sourceBitmaps[5] = BitmapFactory.decodeResource(getResources(), R.drawable.for_histogram256);
        sourceBitmaps[6] = BitmapFactory.decodeResource(getResources(), R.drawable.grid256);
        sourceBitmaps[7] = BitmapFactory.decodeResource(getResources(), R.drawable.lena256);
        sourceBitmaps[8] = BitmapFactory.decodeResource(getResources(), R.drawable.lena_distortions256);
        sourceBitmaps[9] = BitmapFactory.decodeResource(getResources(), R.drawable.mandrill256);
        sourceBitmaps[10] = BitmapFactory.decodeResource(getResources(), R.drawable.monkey256);
        sourceBitmaps[11] = BitmapFactory.decodeResource(getResources(), R.drawable.parrots256);
        sourceBitmaps[12] = BitmapFactory.decodeResource(getResources(), R.drawable.tire256);

        menuButtons = new Button[3];
        menuButtons[0] = new Button(this);
        menuButtons[0].setText("Clear");
        menuButtons[0].setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        CleanViews();
                    }
                }
        );

        menuButtons[1] = new Button(this);
        menuButtons[1].setText("Roll image");
        menuButtons[1].setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        bitmap = ++bitmap % 13;
                        for (int i = 0; i < views.size(); i++) {
                            views.get(i).setImageBitmap(sourceBitmaps[bitmap]);
                        }
                        step = 0;
                    }
                }
        );

        menuButtons[2] = new Button(this);
        menuButtons[2].setText("Paste original");
        menuButtons[2].setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        PasteOriginal();
                    }
                }
        );


        for (int i = 0; i < menuButtons.length; i++) {
            controlButtonsTable.addView(menuButtons[i]);
        }

        filtersSimple = new ArrayList<>();
        filtersSimple.add(BSSLeftToRight.class);
        filtersSimple.add(BSSRotate90.class);
        filtersSimple.add(BSSSine.class);
        filtersSimple.add(BSSCosine.class);
        filtersSimple.add(BSSRedFilter.class);
        filtersSimple.add(BSSBlueFilter.class);
        filtersSimple.add(BSSGreenFilter.class);
        filtersSimple.add(BSSHistogram.class);
        filtersSimple.add(BSSEqualize.class);
        filtersSimple.add(BSSTintWarm.class);
        filtersSimple.add(BSSAbberation.class);
        filtersSimple.add(BSSQuantum.class);
        filtersSimple.add(BSSGammaCompression.class);
        filtersSimple.add(BSSGammaExpansion.class);


        filtersComplex = new ArrayList<>();
        filtersComplex.add(new Pair<>(new Pair<Class, String>(BSSRotateTheta.class,"15°"), new Object[]{new Object[]{15}}));
        filtersComplex.add(new Pair<>(new Pair<Class, String>(BSSRotateTheta.class,"20°"), new Object[]{new Object[]{20}}));
        filtersComplex.add(new Pair<>(new Pair<Class, String>(BSSRotateTheta.class,"35°"), new Object[]{new Object[]{35}}));

        filtersComplex.add(new Pair<>(new Pair<Class, String>(BSSBlackWhite.class,"2'"), new Object[]{new Object[]{2}}));
        filtersComplex.add(new Pair<>(new Pair<Class, String>(BSSBlackWhite.class,"3'"), new Object[]{new Object[]{3}}));
        filtersComplex.add(new Pair<>(new Pair<Class, String>(BSSBlackWhite.class,"4'"), new Object[]{new Object[]{4}}));
        filtersComplex.add(new Pair<>(new Pair<Class, String>(BSSBlackWhite.class,"8'"), new Object[]{new Object[]{8}}));
        filtersComplex.add(new Pair<>(new Pair<Class, String>(BSSBlackWhite.class,"16'"), new Object[]{new Object[]{16}}));

        filtersComplex.add(new Pair<>(new Pair<Class, String>(BSSMedian.class,"8x8"), new Object[]{new Object[]{4}}));
        filtersComplex.add(new Pair<>(new Pair<Class, String>(BSSMedian.class,"16x16"), new Object[]{new Object[]{16}}));
        filtersComplex.add(new Pair<>(new Pair<Class, String>(BSSMedian.class,"32x32"), new Object[]{new Object[]{32}}));

        filtersComplex.add(new Pair<>(new Pair<Class, String>(BSSZoomOut.class," center 2x2"), new Object[]{
                new Object[]{
                        new Object[]{0.5d},
                        new Object[]{0.5d},
                        new Object[]{2d},
                        new Object[]{2d}
                }
        }));

        filtersComplex.add(new Pair<>(new Pair<Class, String>(BSSZoomOut.class," center 3x3"), new Object[]{
                new Object[]{
                        new Object[]{0.5d},
                        new Object[]{0.5d},
                        new Object[]{3d},
                        new Object[]{3d}
                }
        }));

        filtersComplex.add(new Pair<>(new Pair<Class, String>(BSSZoomIn.class," center 2x2"), new Object[]{
                new Object[]{
                        new Object[]{0.5d},
                        new Object[]{0.5d},
                        new Object[]{2d},
                        new Object[]{2d}
                }
        }));

        filtersComplex.add(new Pair<>(new Pair<Class, String>(BSSZoomIn.class," center 2.5x2.5"), new Object[]{
                new Object[]{
                        new Object[]{0.5d},
                        new Object[]{0.5d},
                        new Object[]{2.5d},
                        new Object[]{2.5d}
                }
        }));

        filtersComplex.add(new Pair<>(new Pair<Class, String>(BSSZoomIn.class," center 2x3"), new Object[]{
                new Object[]{
                        new Object[]{0.5d},
                        new Object[]{0.5d},
                        new Object[]{2d},
                        new Object[]{3d}
                }
        }));

        filtersComplex.add(new Pair<>(new Pair<Class, String>(BSSZoomIn.class," center 3x3"), new Object[]{
                new Object[]{
                        new Object[]{0.5d},
                        new Object[]{0.5d},
                        new Object[]{3d},
                        new Object[]{3d}
                }
        }));

        filtersComplex.add(new Pair<>(new Pair<Class, String>(BSSZoomIn.class," left 2x2"), new Object[]{
                new Object[]{
                        new Object[]{0d},
                        new Object[]{0.5d},
                        new Object[]{2d},
                        new Object[]{2d}
                }
        }));

        filtersComplex.add(new Pair<>(new Pair<Class, String>(BSSZoomIn.class," bottom 2x2"), new Object[]{
                new Object[]{
                        new Object[]{0.5d},
                        new Object[]{1d},
                        new Object[]{2d},
                        new Object[]{2d}
                }
        }));

        filtersComplex.add(new Pair<>(new Pair<Class, String>(BSSZoomIn.class," right 2x2"), new Object[]{
                new Object[]{
                        new Object[]{1d},
                        new Object[]{0.5d},
                        new Object[]{2d},
                        new Object[]{2d}
                }
        }));

        filtersComplex.add(new Pair<>(new Pair<Class, String>(BSSZoomIn.class," top 2x2"), new Object[]{
                new Object[]{
                        new Object[]{0.5d},
                        new Object[]{0d},
                        new Object[]{2d},
                        new Object[]{2d}
                }
        }));


        filtersComplex.add(new Pair<>(new Pair<Class, String>(BSSKernel.class," edges"), new Object[]{
                new Object[]{
                        new Object[]{3},
                        new Object[]{3},
                        new Object[]{-1.0, -1.0, -1.0, -1.0, 8.0, -1.0, -1.0, -1.0, -1.0}
                }
        }));

        filtersComplex.add(new Pair<>(new Pair<Class, String>(BSSKernel.class," blur gaussian"), new Object[]{
                new Object[]{
                        new Object[]{3},
                        new Object[]{3},
                        new Object[]{0.0625, 0.125, 0.0625, 0.125, 0.25, 0.125, 0.0625, 0.125, 0.0625}
                }
        }));

        filtersComplex.add(new Pair<>(new Pair<Class, String>(BSSKernel.class," blur box"), new Object[]{
                new Object[]{
                        new Object[]{3},
                        new Object[]{3},
                        new Object[]{0.111, 0.111, 0.111, 0.111, 0.111, 0.111, 0.111, 0.111, 0.111}
                }
        }));

        filtersComplex.add(new Pair<>(new Pair<Class, String>(BSSKernel.class," blur fine"), new Object[]{
                new Object[]{
                        new Object[]{5},
                        new Object[]{5},
                        new Object[]{
                                0.026315789, 0.026315789, 0.052631579, 0.026315789, 0.026315789,
                                0.026315789, 0.052631579, 0.052631579, 0.052631579, 0.026315789,
                                0.052631579, 0.052631579, 0.052631579, 0.052631579, 0.052631579,
                                0.026315789, 0.052631579, 0.052631579, 0.052631579, 0.026315789,
                                0.026315789, 0.026315789, 0.052631579, 0.026315789, 0.026315789
                        }
                }
        }));

        filtersComplex.add(new Pair<>(new Pair<Class, String>(BSSKernel.class," sharpen"), new Object[]{
                new Object[]{
                        new Object[]{3},
                        new Object[]{3},
                        new Object[]{0.0, -1.0, 0.0, -1.0, 5.0, -1.0, 0.0, -1.0, 0.0}
                }
        }));

        filtersComplex.add(new Pair<>(new Pair<Class, String>(BSSKernel.class," sharpen hard"), new Object[]{
                new Object[]{
                        new Object[]{5},
                        new Object[]{5},
                        new Object[]{
                                 0.0,  0.0, -1.0,  0.0,  0.0,
                                 0.0, -1.0, -1.0, -1.0,  0.0,
                                -1.0, -1.0,  13.0, -1.0, -1.0,
                                 0.0, -1.0, -1.0, -1.0,  0.0,
                                 0.0,  0.0, -1.0,  0.0,  0.0
                        }
                }
        }));

        filtersComplex.add(new Pair<>(new Pair<Class, String>(BSSKernel.class," emboss"), new Object[]{
                new Object[]{
                        new Object[]{5},
                        new Object[]{5},
                        new Object[]{
                                -3.0, -2.0, -1.0,  0.0,  0.0,
                                -2.0, -1.0, -1.0,  0.0,  0.0,
                                -1.0, -1.0,  1.0,  1.0,  1.0,
                                 0.0,  0.0,  1.0,  1.0,  2.0,
                                 0.0,  0.0,  1.0,  2.0,  3.0
                        }
                }
        }));

        filtersComplex.add(new Pair<>(new Pair<Class, String>(BSSKernel.class," clarify"), new Object[]{
                new Object[]{
                        new Object[]{5},
                        new Object[]{5},
                        new Object[]{
                                 0.0,  0.0, -0.25, 0.0,  0.0,
                                 0.0, -0.5,  1.0, -0.5,  0.0,
                                -0.25, 1.0,  0.0,  1.0, -0.25,
                                 0.0, -0.5,  1.0, -0.5,  0.0,
                                 0.0,  0.0, -0.25, 0.0,  0.0
                        }
                }
        }));

        filtersComplex.add(new Pair<>(new Pair<Class, String>(BSSKernel.class," median"), new Object[]{
                new Object[]{
                        new Object[]{5},
                        new Object[]{5},
                        new Object[]{
                                 0.04, 0.04, 0.04, 0.04, 0.04,
                                 0.04, 0.04, 0.04, 0.04, 0.04,
                                 0.04, 0.04, 0.04, 0.04, 0.04,
                                 0.04, 0.04, 0.04, 0.04, 0.04,
                                 0.04, 0.04, 0.04, 0.04, 0.04
                        }
                }
        }));

        controlButtons = new Button[filtersSimple.size() + filtersComplex.size()];

         for (int i = 0; i < filtersSimple.size(); i++){
             final Class tfilter = filtersSimple.get(i);
             controlButtons[i] = new Button(this);
             controlButtons[i].setText(tfilter.getSimpleName());
               controlButtons[i].setOnClickListener(
                   new View.OnClickListener() {
                       @Override
                       public void onClick(View view) {
                           ProcessWithFilter(tfilter);
                       }
                   }
               );
             controlButtonsTable.addView(controlButtons[i]);
         }

        int t_reindexer = filtersSimple.size();
        for (int i = t_reindexer; i < filtersSimple.size() + filtersComplex.size(); i++){
            final Class tfilter = filtersComplex.get(i - t_reindexer).first.first;
            final Object[] tdata = filtersComplex.get(i - t_reindexer).second;
            controlButtons[i] = new Button(this);
            controlButtons[i].setText(tfilter.getSimpleName()+":"+filtersComplex.get(i - t_reindexer).first.second);

            controlButtons[i].setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ProcessWithFilter(tfilter,tdata);
                        }
                    }
            );
            controlButtonsTable.addView(controlButtons[i]);
        }

        for (int i = 0; i < 3; i++) {
            views.add(new ImageView(this));
            views.get(i).setImageBitmap(sourceBitmaps[bitmap]);
            imageViewsGrid.addView(views.get(i), width, height);
        }

        LinearLayout.LayoutParams LPLeft = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT );
        LPLeft.setMarginStart(24);
        LPLeft.setMarginEnd(24);

        LinearLayout.LayoutParams LPRight = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT );
        LPRight.setMarginStart(48);
        LPRight.setMarginEnd(0);

        buttonsScroller.addView(controlButtonsTable);
        buttonsScroller.setLayoutParams(LPLeft);
        mainLayout.addView(buttonsScroller);

        imageViewsScroller.addView(imageViewsGrid);
        imageViewsScroller.setLayoutParams(LPRight);
        mainLayout.addView(imageViewsScroller);

        setContentView(mainLayout);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    }

    protected void CleanViews(){
        views.clear();
        imageViewsGrid.removeAllViews();
        for (int i = 0; i < 3; i++) {
            views.add(new ImageView(this));
            views.get(i).setImageBitmap(sourceBitmaps[bitmap]);
            imageViewsGrid.addView(views.get(i), width, height);
        }
        step = 0;
    }

    protected void PasteOriginal(){
        ++step;
        if (step >= views.size() - 1) {
            int i = views.size();
            views.add(new ImageView(this));
            views.get(i).setImageBitmap(sourceBitmaps[bitmap]);
            imageViewsGrid.addView(views.get(i), width, height);
        }
        else {
            views.get(step).setImageBitmap(sourceBitmaps[bitmap]);
        }
    }

    protected void ProcessWithFilter(Class filter){
        try {

            if (!(BitmapSolverSlave.class.isAssignableFrom(filter))) {
                Toast.makeText(this, "Tried with wrong class", Toast.LENGTH_LONG).show();
                return;
            }

            if (step == views.size() - 1) {
                Toast.makeText(this, "All views were used, new one is added below", Toast.LENGTH_SHORT).show();
                int i = views.size();
                views.add(new ImageView(this));
                views.get(i).setImageBitmap(sourceBitmaps[bitmap]);
                imageViewsGrid.addView(views.get(i), width, height);
            }


            ImageView in = views.get(step);
            ImageView out = views.get(++step);
            AsyncRunner runner = new AsyncRunner(in, out);
            Runners.add(new DelayedRunner(runner,filter));

            if (isProcessing) {
                Toast.makeText(this, "Added to queue at position "+Runners.size()+" on view "+step, Toast.LENGTH_SHORT).show();
                return;
            }

            //TODO: Make interactive stack for filtersSimple
            Runners.remove().Execute();

        } catch (Exception e){
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    protected void ProcessWithFilter(Class filter, Object[] data){
        try {

            if (!(BitmapSolverSlave.class.isAssignableFrom(filter))) {
                Toast.makeText(this, "Tried with wrong class", Toast.LENGTH_LONG).show();
                return;
            }

            if (step == views.size() - 1) {
                Toast.makeText(this, "All views were used, new one is added below", Toast.LENGTH_SHORT).show();
                int i = views.size();
                views.add(new ImageView(this));
                views.get(i).setImageBitmap(sourceBitmaps[bitmap]);
                imageViewsGrid.addView(views.get(i), width, height);
            }

            ImageView in = views.get(step);
            ImageView out = views.get(++step);
            AsyncRunner runner = new AsyncRunner(in, out, data);
            Runners.add(new DelayedRunner(runner,filter));

            if (isProcessing) {
                Toast.makeText(this, "Added to queue at position "+Runners.size()+" on view "+step, Toast.LENGTH_SHORT).show();
                return;
            }

            //TODO: Make interactive stack for filtersSimple
            Runners.remove().Execute();

        } catch (Exception e){
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
