package com.thebeards.inshadow.imageprocessing.Multithreading.ColorChannelEffects;

import com.thebeards.inshadow.imageprocessing.ColorSupport.ColorSupport;
import com.thebeards.inshadow.imageprocessing.Multithreading.BitmapSolverSlave;

/**
 * Created by INshadow on 22.10.2017.
 */

public class BSSTintWarm extends BitmapSolverSlave {
    @Override
    protected void pixel(int x, int y){
        int channels[] = ColorSupport.ToComponents(workingSet[x + (y*width)]);
        presentingSet[x + (y*width)] = ColorSupport.ToColor((int)(channels[0] * 1.05f),(int)(channels[1] * 0.95f),(int)(channels[2] * 0.88f),channels[3]);
    }
}
