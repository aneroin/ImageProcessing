package com.thebeards.inshadow.imageprocessing.Multithreading.PixelPlacementEffects;

import com.thebeards.inshadow.imageprocessing.ColorSupport.ColorSupport;
import com.thebeards.inshadow.imageprocessing.Multithreading.BitmapSolverSlave;

/**
 * Created by INshadow on 11.11.2017.
 */

public class BSSZoomIn extends BitmapSolverSlave {

    protected static int center_Y;
    protected static int center_X;
    protected static double zoom_X;
    protected static double zoom_Y;

    double tx, ty;
    double px_0, px_1, py_0, py_1;
    int tcolor_0, tcolor_1;
    int color0, color1, color2, color3, color4, color5;
    int[] tChannels0, tChannels1;

    public BSSZoomIn(Object[] data){
        super.data = data;
    }

    @Override
    public void Prepare(){
        double focus_X = (double) ((Object[])data[0])[0];
        double focus_Y = (double) ((Object[])data[1])[0];

        focus_X = Math.min(1d, Math.max(0d, focus_X));
        focus_Y = Math.min(1d, Math.max(0d, focus_Y));

        center_X = (int)(focus_X * width);
        center_Y = (int)(focus_Y * height);

        zoom_X = (double) ((Object[])data[2])[0];
        zoom_Y = (double) ((Object[])data[3])[0];

        zoom_X = Math.max(1d, zoom_X);
        zoom_Y = Math.max(1d, zoom_Y);
    }

    @Override
    protected void pixel(int x, int y) {
        //tx = (x / zoom_X) + ( (x - center_X) / zoom_X);
        //ty = (y / zoom_Y) + ( (y - center_Y) / zoom_Y);

        //tx = ((2 * x) + (center_X - x)) /zoom_X;
        //ty = ((2 * y) + (center_Y - y)) / zoom_Y;

        tx = (x + center_X) / zoom_X;
        ty = (y + center_Y) / zoom_Y;

        if (tx < 0 || tx > width || ty < 0 || ty > height)
            return;

        px_0 = Math.floor(tx);
        px_1 = Math.ceil(tx);
        py_0 = Math.floor(ty);
        py_1 = Math.ceil(ty);

        color0 = workingSet[(int)(px_0 + py_0*width)];
        color1 = workingSet[(int)(px_1 + py_0*width)];
        color2 = workingSet[(int)(px_0 + py_1*width)];
        color3 = workingSet[(int)(px_1 + py_1*width)];

        if (px_0 == tx && px_1 == tx) {
            tChannels0 = ColorSupport.ToComponents(color0);
            tChannels1 = ColorSupport.ToComponents(color1);
            color4 = ColorSupport.ToColor(
                    (int)(tChannels0[0] * 0.5d + tChannels1[0] * 0.5d),
                    (int)(tChannels0[1] * 0.5d + tChannels1[1] * 0.5d),
                    (int)(tChannels0[2] * 0.5d + tChannels1[2] * 0.5d),
                    (int)(tChannels0[3] * 0.5d + tChannels1[3] * 0.5d)
            );

            tChannels0 = ColorSupport.ToComponents(color2);
            tChannels1 = ColorSupport.ToComponents(color3);
            color5 = ColorSupport.ToColor(
                    (int)(tChannels0[0] * 0.5d + tChannels1[0] * 0.5d),
                    (int)(tChannels0[1] * 0.5d + tChannels1[1] * 0.5d),
                    (int)(tChannels0[2] * 0.5d + tChannels1[2] * 0.5d),
                    (int)(tChannels0[3] * 0.5d + tChannels1[3] * 0.5d)
            );
        } else {

            tChannels0 = ColorSupport.ToComponents(color0);
            tChannels1 = ColorSupport.ToComponents(color1);
            color4 = ColorSupport.ToColor(
                    (int) ((tChannels0[0] * ((px_1 - tx) / (px_1 - px_0))) + (tChannels1[0] * ((tx - px_0) / (px_1 - px_0)))),
                    (int) ((tChannels0[1] * ((px_1 - tx) / (px_1 - px_0))) + (tChannels1[1] * ((tx - px_0) / (px_1 - px_0)))),
                    (int) ((tChannels0[2] * ((px_1 - tx) / (px_1 - px_0))) + (tChannels1[2] * ((tx - px_0) / (px_1 - px_0)))),
                    (int) ((tChannels0[3] * ((px_1 - tx) / (px_1 - px_0))) + (tChannels1[3] * ((tx - px_0) / (px_1 - px_0))))
            );


            tChannels0 = ColorSupport.ToComponents(color2);
            tChannels1 = ColorSupport.ToComponents(color3);
            color5 = ColorSupport.ToColor(
                    (int) ((tChannels0[0] * ((px_1 - tx) / (px_1 - px_0))) + (tChannels1[0] * ((tx - px_0) / (px_1 - px_0)))),
                    (int) ((tChannels0[1] * ((px_1 - tx) / (px_1 - px_0))) + (tChannels1[1] * ((tx - px_0) / (px_1 - px_0)))),
                    (int) ((tChannels0[2] * ((px_1 - tx) / (px_1 - px_0))) + (tChannels1[2] * ((tx - px_0) / (px_1 - px_0)))),
                    (int) ((tChannels0[3] * ((px_1 - tx) / (px_1 - px_0))) + (tChannels1[3] * ((tx - px_0) / (px_1 - px_0))))
            );

        }

        tChannels0 = ColorSupport.ToComponents(color4);
        tChannels1 = ColorSupport.ToComponents(color5);

        if (py_0 == ty && py_1 == ty){
            presentingSet[x + y*width]  = ColorSupport.ToColor(
                    (int)(tChannels0[0] * 0.5d + tChannels1[0] * 0.5d),
                    (int)(tChannels0[1] * 0.5d + tChannels1[1] * 0.5d),
                    (int)(tChannels0[2] * 0.5d + tChannels1[2] * 0.5d),
                    (int)(tChannels0[3] * 0.5d + tChannels1[3] * 0.5d)
            );
        } else {
            presentingSet[x + y * width] = ColorSupport.ToColor(
                    (int) ((tChannels0[0] * ((py_1 - ty) / (py_1 - py_0))) + (tChannels1[0] * ((ty - py_0) / (py_1 - py_0)))),
                    (int) ((tChannels0[1] * ((py_1 - ty) / (py_1 - py_0))) + (tChannels1[1] * ((ty - py_0) / (py_1 - py_0)))),
                    (int) ((tChannels0[2] * ((py_1 - ty) / (py_1 - py_0))) + (tChannels1[2] * ((ty - py_0) / (py_1 - py_0)))),
                    (int) ((tChannels0[3] * ((py_1 - ty) / (py_1 - py_0))) + (tChannels1[3] * ((ty - py_0) / (py_1 - py_0))))
            );
        }
    }
}
