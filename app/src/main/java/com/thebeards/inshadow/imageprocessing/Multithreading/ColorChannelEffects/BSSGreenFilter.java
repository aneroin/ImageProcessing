package com.thebeards.inshadow.imageprocessing.Multithreading.ColorChannelEffects;
import com.thebeards.inshadow.imageprocessing.ColorSupport.ColorSupport;
import com.thebeards.inshadow.imageprocessing.Multithreading.BitmapSolverSlave;

/**
 * Created by INshadow on 09.10.2017.
 */

public class BSSGreenFilter extends BitmapSolverSlave {
    @Override
    protected void pixel(int x, int y){
        int channels[] = ColorSupport.ToComponents(workingSet[x + (y*width)]);
        presentingSet[x + (y*width)] = ColorSupport.ToColor(channels[1],channels[1],channels[1],channels[3]);
    }
}
