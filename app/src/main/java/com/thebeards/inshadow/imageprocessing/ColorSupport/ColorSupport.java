package com.thebeards.inshadow.imageprocessing.ColorSupport;

import android.graphics.Color;

/**
 * Created by INshadow on 09.10.2017.
 */

public class ColorSupport {

    public static int ToColor(int r, int g, int b){
        return Color.rgb(
                Math.max(Math.min(r,255),0),
                Math.max(Math.min(g,255),0),
                Math.max(Math.min(b,255),0));
    }

    public static int ToColor(int r, int g, int b, int a){
        return Color.argb(
                Math.max(Math.min(a,255),0),
                Math.max(Math.min(r,255),0),
                Math.max(Math.min(g,255),0),
                Math.max(Math.min(b,255),0)
        );
    }

    public static int[] ToComponents(int color){
        int[] components = new int[4];
        components[0] = Color.red(color);
        components[1] = Color.green(color);
        components[2] = Color.blue(color);
        components[3] = Color.alpha(color);
        return components;
    }

    public static int Luminance(int color){
        int luminance = 0;
        luminance += (int)(Color.red(color) * 0.2126d);
        luminance += (int)(Color.green(color) * 0.7152d);
        luminance += (int)(Color.blue(color) * 0.0722d);
        return luminance;
    }



}
