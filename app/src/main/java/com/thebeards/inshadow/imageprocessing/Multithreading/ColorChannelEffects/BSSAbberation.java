package com.thebeards.inshadow.imageprocessing.Multithreading.ColorChannelEffects;

import com.thebeards.inshadow.imageprocessing.ColorSupport.ColorSupport;
import com.thebeards.inshadow.imageprocessing.Multithreading.BitmapSolverSlave;

/**
 * Created by INshadow on 03.11.2017.
 */

public class BSSAbberation extends BitmapSolverSlave {
    @Override
    protected void pixel(int x, int y){
        int channels[] = ColorSupport.ToComponents(workingSet[x + (y*width)]);
        int channels_r[] = ColorSupport.ToComponents(workingSet[(width + (x + 4)) % width + (y*width)]);
        int channels_l[] = ColorSupport.ToComponents(workingSet[(width + (x - 4)) % width + (y*width)]);
        presentingSet[x + (y*width)] = ColorSupport.ToColor(channels_r[0],channels_l[1],channels[2],channels[3]);
    }
}
