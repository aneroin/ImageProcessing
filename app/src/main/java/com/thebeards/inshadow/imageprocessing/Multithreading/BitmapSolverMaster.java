package com.thebeards.inshadow.imageprocessing.Multithreading;

/**
 * Created by INshadow on 09.10.2017.
 */

public class BitmapSolverMaster extends Thread {

    private BitmapSolverSlave[] slaves;
    private String name;

    public BitmapSolverMaster(String name){
        this.name = name;
        System.out.println(String.format("Created %s bitmap solver",name));
    }

    /**
     *
     * @param input
     * @param threadsCount should be power of 2
     * @return
     */
    public int[] Solve(int[] input, int threadsCount, BitmapSolverSlave[] slaves) throws Exception {
        double root = Math.sqrt(threadsCount);
        if (root - (long)root != 0d)
            throw new Exception("threadsCount should be in power of two");


        int[] output = new int[input.length];

        int sectorSize = input.length / threadsCount;
        int sectorWidth = (int) Math.sqrt(sectorSize);
        int sectorHeight = sectorSize / sectorWidth;

        System.out.println("processing "+input.length+" pixels, divided into sectors of "+sectorWidth+"x"+sectorHeight);

        long start_ms = System.currentTimeMillis();

        System.out.println("start time "+start_ms%100000);

        for (int j = 0; j < root; j++) {
            for (int i = 0; i < root; i++) {
                // index = x + y*width;
                // x = index - y * width;
                // y = (index - x) / width
                System.out.println("Initialize slave #"+(i + j * (int)root));
                slaves[i + j * (int)root].SetSlice(input, output, sectorWidth * (int)root, sectorHeight * (int)root, i * sectorWidth, j * sectorHeight, (i + 1) * sectorWidth, (j + 1) * sectorHeight);
                if (i == 0 && j == 0){
                    //Prepare entire set from first instance
                    slaves[0].Prepare();
                }
                slaves[i + j * (int)root].run();
            }
        }

        System.out.println("waiting until processed");

        for (int j = 0; j < root; j++) {
            for (int i = 0; i < root; i++) {
                slaves[i + j * (int)root].join();
            }
        }

        slaves[0].Finalize();

        System.out.println("processed "+output.length+" pixels");

        long end_ms =System.currentTimeMillis();
        System.out.println("end time "+end_ms%100000);

        System.out.println("processed in "+ (end_ms - start_ms));

        return output;
    }
}
