package com.thebeards.inshadow.imageprocessing.Multithreading.ColorChannelEffects;

import com.thebeards.inshadow.imageprocessing.ColorSupport.ColorSupport;
import com.thebeards.inshadow.imageprocessing.Multithreading.BitmapSolverSlave;

/**
 * Created by INshadow on 03.11.2017.
 */

public class BSSBlackWhite extends BitmapSolverSlave {

    int levels;
    int thCoefficient;

    public BSSBlackWhite(Object[] data){
        super(data);
        levels = (int) super.data[0];
        thCoefficient = (255 / levels);
    }

    @Override
    protected void pixel(int x, int y){
        int channels[] = ColorSupport.ToComponents(workingSet[x + (y*width)]);
        int luminance = ColorSupport.Luminance(workingSet[x + (y*width)]);
        int color = (int)((luminance / 255d) * levels) * thCoefficient;
        presentingSet[x + (y*width)] = ColorSupport.ToColor(color,color,color,channels[3]);
    }
}
