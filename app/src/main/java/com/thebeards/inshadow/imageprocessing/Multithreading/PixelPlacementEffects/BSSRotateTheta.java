package com.thebeards.inshadow.imageprocessing.Multithreading.PixelPlacementEffects;

import com.thebeards.inshadow.imageprocessing.ColorSupport.ColorSupport;
import com.thebeards.inshadow.imageprocessing.Multithreading.BitmapSolverSlave;

/**
 * Created by INshadow on 22.10.2017.
 */

public class BSSRotateTheta extends BitmapSolverSlave {

    public BSSRotateTheta(Object[] data){
        super.data = data;
    }

    @Override
    protected void pixel(int x, int y){
        int theta = (int) data[0];
        int channels[];
        double cost, sint;
        cost = Math.cos( -theta * Math.PI / 180.0 );
        sint = Math.sin( -theta * Math.PI / 180.0 );
        int xs = width / 2;
        int ys = height / 2;

        int x1 = (int) ( xs + ( (x - xs) * cost - (y - ys) * sint + 0.5 ) );
        int y1 = (int) ( ys + ( (x - xs) * sint + (y - ys) * cost + 0.5 ) );

        if ( ( x1 >= 0 ) && ( x1 < width ) && ( y1 >= 0 ) && ( y1 < height ) )
            channels = ColorSupport.ToComponents(workingSet[x1 + (y1*width)]);
        else
            channels = new int[]{255, 127, 255, 127};

        presentingSet[x + (y*width)] = ColorSupport.ToColor(channels[0],channels[1],channels[2],channels[3]);
    }
}
