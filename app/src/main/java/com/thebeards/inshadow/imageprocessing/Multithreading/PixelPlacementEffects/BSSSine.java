package com.thebeards.inshadow.imageprocessing.Multithreading.PixelPlacementEffects;

import android.util.Log;

import com.thebeards.inshadow.imageprocessing.ColorSupport.ColorSupport;
import com.thebeards.inshadow.imageprocessing.Multithreading.BitmapSolverSlave;

/**
 * Created by INshadow on 03.11.2017.
 */

public class BSSSine extends BitmapSolverSlave {

    @Override
    protected void pixel(int x, int y) {

        int ny = (int) (y + (((1d + Math.sin(Math.toRadians(x))) * height * 0.025d) % height)) % height;
        presentingSet[x + y*width] = workingSet[x + ny*width];
    }
}
