package com.thebeards.inshadow.imageprocessing.Multithreading.ConvolutionEffects;

import com.thebeards.inshadow.imageprocessing.ColorSupport.ColorSupport;
import com.thebeards.inshadow.imageprocessing.Multithreading.BitmapSolverSlave;

/**
 * Created by INshadow on 23.10.2017.
 */

public class BSSKernel extends BitmapSolverSlave {

    int cw;
    int ch;
    double[] matrix;
    int[] components;
    int j,i;
    int R,G,B;

    public BSSKernel(Object[] data){
        super(data);
        cw = (int) ((Object[])data[0])[0];
        ch = (int) ((Object[])data[1])[0];
        matrix = new double[ch * cw];
        for (int k = 0; k < ch; k++)
            for (int l = 0; l < cw; l++){
                matrix[l + k * cw] = (double)((Object[])data[2])[l + k*cw];
            }
    }

    @Override
    protected void pixel(int x, int y) {
        R = G = B = 0;
        for (int k = 0; k < ch; k++) {
            for (int l = 0; l < cw; l++) {
                j = Math.max(y - ch/2 - 1 + k,0) % height;
                i = Math.max(x - cw/2 -1 + l,0) % width;
                components = ColorSupport.ToComponents(workingSet[i + j*width]);
                R += components[0] * matrix[l + k*cw];
                G += components[1] * matrix[l + k*cw];
                B += components[2] * matrix[l + k*cw];
            }
        }
        presentingSet[x + y*width] = ColorSupport.ToColor(R,G,B,ColorSupport.ToComponents(workingSet[x + y*width])[3]);
    }
}
