package com.thebeards.inshadow.imageprocessing.Multithreading.ColorChannelEffects;

import com.thebeards.inshadow.imageprocessing.ColorSupport.ColorSupport;
import com.thebeards.inshadow.imageprocessing.Multithreading.BitmapSolverSlave;

/**
 * Created by INshadow on 11.11.2017.
 */

public class BSSEqualize extends BitmapSolverSlave {
    static int[] R_Histogram;
    static int[] G_Histogram;
    static int[] B_Histogram;

    int[] channels;

    protected synchronized void IncR(int i){
        R_Histogram[i] += 1;
    }

    protected synchronized void IncG(int i){
        G_Histogram[i] += 1;
    }

    protected synchronized void IncB(int i){
        B_Histogram[i] += 1;
    }

    public BSSEqualize(){
        channels = new int[4];
        R_Histogram = new int[256];
        G_Histogram = new int[256];
        B_Histogram = new int[256];
    }

    @Override
    protected void pixel(int x, int y){
        channels = ColorSupport.ToComponents(workingSet[x + (y*width)]);
        IncR(channels[0]);
        IncG(channels[1]);
        IncB(channels[2]);
    }

    @Override
    public void Finalize(){
        super.Finalize();

        int maxR, maxG, maxB, minR, minG, minB;
        maxR = maxG = maxB = 0;
        minR = minG = minB = Integer.MAX_VALUE;
        for (int i = 0; i < 255; i++){
            maxR = R_Histogram[i] > maxR ? R_Histogram[i] : maxR;
            minR = R_Histogram[i] < minR && R_Histogram[i] > 0 ? R_Histogram[i] : minR;
            maxG = G_Histogram[i] > maxG ? G_Histogram[i] : maxG;
            minG = G_Histogram[i] < minG && G_Histogram[i] > 0 ? G_Histogram[i] : minG;
            maxB = B_Histogram[i] > maxB ? B_Histogram[i] : maxB;
            minB = B_Histogram[i] < minB && B_Histogram[i] > 0 ? B_Histogram[i] : minB;
        }

        int vR, vG, vB;

        for (int j = 0; j < height; j++) {
            for (int i = 0; i < width; i++) {
                vR = vG = vB = 0;
                for (int v = 0; v < channels[0]; v++){
                    vR += R_Histogram[v];
                }
                for (int v = 0; v < channels[1]; v++){
                    vG += G_Histogram[v];
                }
                for (int v = 0; v < channels[2]; v++){
                    vB += B_Histogram[v];
                }

                channels = ColorSupport.ToComponents(workingSet[i + (j*width)]);
                paint(i, j, ColorSupport.ToColor(
                        (int) (((vR - minR) / ((width * height) -1d)) * 255),
                        (int) (((vG - minG) / ((width * height) -1d)) * 255),
                        (int) (((vB - minB) / ((width * height) -1d)) * 255),
                        channels[3]));
            }
        }
    }

    protected void paint(int x, int y, int color){
        presentingSet[x + (y*width)] = color;
    }
}
