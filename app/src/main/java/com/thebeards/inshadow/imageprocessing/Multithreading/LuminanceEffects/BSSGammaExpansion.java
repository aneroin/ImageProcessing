package com.thebeards.inshadow.imageprocessing.Multithreading.LuminanceEffects;

import com.thebeards.inshadow.imageprocessing.ColorSupport.ColorSupport;
import com.thebeards.inshadow.imageprocessing.Multithreading.BitmapSolverSlave;

/**
 * Created by INshadow on 14.10.2017.
 */

public class BSSGammaExpansion extends BitmapSolverSlave {

    @Override
    protected void pixel(int x, int y){
        int channels[] = ColorSupport.ToComponents(workingSet[x + y*width]);

        double nR, nG, nB;
        nR = channels[0] / 255d;
        nG = channels[1] / 255d;
        nB = channels[2] / 255d;

        int[] gamma = new int[3];
        gamma[0] = (int)(255 * Math.pow(nR,2));
        gamma[1] = (int)(255 * Math.pow(nG,2));
        gamma[2] = (int)(255 * Math.pow(nB,2));

        presentingSet[x + y*width] = ColorSupport.ToColor(gamma[0],gamma[1],gamma[2],channels[3]);
    }

}
