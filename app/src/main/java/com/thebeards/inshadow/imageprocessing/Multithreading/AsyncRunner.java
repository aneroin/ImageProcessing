package com.thebeards.inshadow.imageprocessing.Multithreading;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import com.thebeards.inshadow.imageprocessing.MainActivity;

import java.lang.reflect.Constructor;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;


/**
 * Created by INshadow on 09.10.2017.
 */

public class AsyncRunner extends AsyncTask<Class, int[], Void> {

    int width, height;
    int[] output;
    int[] colors;

    Object[] data;

    ImageView inputImg;
    ImageView outputImg;

    public AsyncRunner(ImageView input, ImageView output) throws Exception {
        inputImg = input;
        outputImg = output;
    }

    public AsyncRunner(ImageView input, ImageView output, Object[] payload) throws Exception {
        inputImg = input;
        outputImg = output;
        data = payload;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        MainActivity.SetProcessing(true);

        System.out.println("==================================================\r\n\r\n\r\n");
        System.out.println("Starting Process");
        System.out.println("\r\n\r\n\r\n==================================================");
        inputImg.destroyDrawingCache();
        inputImg.buildDrawingCache();
        Bitmap bmp = inputImg.getDrawingCache();
        width = bmp.getWidth();
        height = bmp.getHeight();
        colors = new int[width * height];

        if (bmp == null){
            System.out.println("==================================================\r\n\n\n");
            System.out.println("bmp is null");
            System.out.println("\r\n\r\n\r\n==================================================");
        } else {
            System.out.println("==================================================\r\n\n\n");
            System.out.println("bmp size is "+bmp.getWidth()+"x"+bmp.getHeight());
            System.out.println("\r\n\r\n\r\n==================================================");
        }

        bmp.getPixels(colors,0,width,0,0,width,height);

        synchronized (outputImg) {
            outputImg.setImageBitmap(bmp);
        }
    }

    void UpdateWorkingPixels() {
        colors = output;
    }


    @Override
    protected Void doInBackground(Class... classes) {
        for (int i = 0; i < classes.length; i++) {
            BitmapSolverMaster master = new BitmapSolverMaster(classes[i].getSimpleName()+" Master");
            BitmapSolverSlave[] slaves = new BitmapSolverSlave[16];
            try {
                Constructor<?> constructor;
                if (data == null) {
                    constructor = classes[i].getConstructor();
                    for (int j = 0; j < slaves.length; j++) {
                        slaves[j] = (BitmapSolverSlave) constructor.newInstance();
                    }
                }
                else {
                    constructor = classes[i].getConstructor(Object[].class);
                    for (int j = 0; j < slaves.length; j++) {
                        slaves[j] = (BitmapSolverSlave) constructor.newInstance((Object[])data);
                    }
                }

                output = master.Solve(colors, slaves.length, slaves);
                UpdateWorkingPixels();
                publishProgress(output);
//                try {
//                    Thread.sleep(500);
//                } catch (InterruptedException ie){
//                    //We don't care of this
//                }
            } catch (Exception e) {
                System.out.println("==================================================\r\n\r\n\r\n");
                System.out.println(e.getMessage());
                System.out.println();
                e.printStackTrace();
                System.out.println("\r\n\r\n==================================================");
                Log.e("Async Runner ","DoInBackgroud: "+e.getMessage());
                for (int j = 0; j < e.getStackTrace().length; j++)
                    Log.e("Async Runner ","trace: "+e.getStackTrace()[j].toString());
                this.cancel(true);
            }
        }
        return null;
    }


    @Override
    protected void onProgressUpdate(int[]... outputs) {
        System.out.println("==================================================\r\n\r\n\r\n");
        System.out.println("image filter applied");
        System.out.println("\r\n\r\n==================================================");
        Bitmap outbmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        outbmp.eraseColor(Color.GREEN);
        outbmp.setPixels(output,0,width,0,0,width,height);
        synchronized (outputImg) {
            outputImg.setImageBitmap(outbmp);
        }
    }

    @Override
    protected void onPostExecute(Void dumb) {
        Bitmap outbmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        outbmp.eraseColor(Color.GREEN);
        outbmp.setPixels(output,0,width,0,0,width,height);
        synchronized (outputImg) {
            outputImg.setImageBitmap(outbmp);
        }

        MainActivity.SetProcessing(false);

        System.out.println("==================================================\r\n\r\n\r\n");
        System.out.println("image processed successfully");
        System.out.println("\r\n\r\n==================================================");
    }
}
